#include <stdio.h>
#include <sys/syscall.h>
#include <time.h>
#include <unistd.h>

void add_diff_to_sum(struct timespec *result, struct timespec a,
                     struct timespec b) {
  if (result->tv_nsec + a.tv_nsec < b.tv_nsec) {
    result->tv_nsec = 1000000000 + result->tv_nsec + a.tv_nsec - b.tv_nsec;
    result->tv_sec = result->tv_sec + a.tv_sec - b.tv_sec - 1;
  } else if (result->tv_nsec + a.tv_nsec - b.tv_nsec >= 1000000000) {
    result->tv_nsec = result->tv_nsec + a.tv_nsec - b.tv_nsec - 1000000000;
    result->tv_sec = result->tv_sec + a.tv_sec - b.tv_sec + 1;
  } else {
    result->tv_nsec = result->tv_nsec + a.tv_nsec - b.tv_nsec;
    result->tv_sec = result->tv_sec + a.tv_sec - b.tv_sec;
  }
}

int main() {
  struct timespec start, end, res, junk;
  // Warmup
  clock_gettime(CLOCK_MONOTONIC, &junk);
  syscall(SYS_getpid);

  // Real tests
  clock_gettime(CLOCK_MONOTONIC, &start);
  syscall(SYS_getpid);
  clock_gettime(CLOCK_MONOTONIC, &end);

  res.tv_sec = 0;
  res.tv_nsec = 0;

  add_diff_to_sum(&res, end, start);

  printf("%ld.%09ld\n", res.tv_sec, res.tv_nsec);
  return 0;
}
