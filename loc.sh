cd $1/sys
git fetch --all --tags
git checkout release/8.3.0
loc=`find . -type f -name '*.c' -exec cat {} \; | grep -a ";" | wc -l`
echo "C statements in FreeBSD 8.3: $loc"
git checkout release/9.2.0
loc=`find . -type f -name '*.c' -exec cat {} \; | grep -a ";" | wc -l`
echo "C statements in FreeBSD 9.2: $loc"
git checkout release/10.2.0
loc=`find . -type f -name '*.c' -exec cat {} \; | grep -a ";" | wc -l`
echo "C statements in FreeBSD 10.2: $loc"
git checkout release/11.1.0
loc=`find . -type f -name '*.c' -exec cat {} \; | grep -a ";" | wc -l`
echo "C statements in FreeBSD 11.1: $loc"
git checkout release/12.1.0
loc=`find . -type f -name '*.c' -exec cat {} \; | grep -a ";" | wc -l`
echo "C statements in FreeBSD 12.1: $loc"
git checkout release/13.0.0
loc=`find . -type f -name '*.c' -exec cat {} \; | grep -a ";" | wc -l`
echo "C statements in FreeBSD 13.0: $loc"






