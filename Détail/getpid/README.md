# Getpid

Exemple d'implémentation de la méthode _Détail_ appliquée à `getpid()`.
Lancer run.sh. Les résultats sont dans getpid\_times.dat.

L'outil ministat permet d'avoir un premier résumé de la distribution.
